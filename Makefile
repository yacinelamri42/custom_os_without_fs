BOOTLOADER = ./src/bootloader/
KERNEL = ./src/kernel/
BUILD_DIR = ./build/

C_SOURCES = $(shell find $(KERNEL) -name *.c)
ASM_SOURCES = $(shell find $(KERNEL) -name *.asm)
HEADERS = $(shell find $(KERNEL) -name *.h)
# Nice syntax for file extension replacement

OBJ = ${C_SOURCES:.c=.o}
ASM_OBJ = $(ASM_SOURCES:.asm=.o)

# Change this if your cross-compiler is somewhere else
ASM = nasm
CC = i686-elf-gcc
GDB = i686-elf-gdb
LD = i686-elf-ld
# -g: Use debugging symbols in gcc
CFLAGS = -g

.PHONY: all disk run debug clean

all: disk

disk: $(BUILD_DIR)/disk.img

$(BUILD_DIR)/disk.img: $(BUILD_DIR)/os-image.bin
	dd if=/dev/zero of=$@ bs=512 count=2880
	dd if=$^ of=$@ conv=notrunc

# First rule is run by default
$(BUILD_DIR)/os-image.bin: $(BOOTLOADER)/bootloader.bin $(BUILD_DIR)/kernel.bin
	cat $^ > $@

# '--oformat binary' deletes all symbols as a collateral, so we don't need
# to 'strip' them manually on this case
$(BUILD_DIR)/kernel.bin: $(KERNEL)/load_kernel.o ${OBJ} $(ASM_OBJ)
	$(LD) -o $@ -Ttext 0x1000 $^ --oformat binary

# Used for debugging purposes
$(BUILD_DIR)/kernel.elf: $(KERNEL)/load_kernel.o ${OBJ} $(ASM_OBJ)
	$(LD) -o $@ -Ttext 0x1000 $^ 

# Open the connection to qemu and load our kernel-object file with symbols
debug: $(BUILD_DIR)/os-image.bin $(BUILD_DIR)/kernel.elf
	qemu-system-i386 -gdb tcp::4444 -fda $(BUILD_DIR)/os-image.bin &
	${GDB} -ex "target remote localhost:4444" -ex "symbol-file $(BUILD_DIR)/kernel.elf"

run: $(BUILD_DIR)/os-image.bin
	qemu-system-i386 -fda $(BUILD_DIR)/os-image.bin

# Generic rules for wildcards
# To make an object, always compile from its .c
%.o: %.c ${HEADERS}
	${CC} ${CFLAGS} -ffreestanding -c $< -o $@

%.o: %.asm
	$(ASM) $< -f elf -o $@

%.bin: %.asm
	$(ASM) $< -f bin -o $@

clean:
	rm -rf $(BUILD_DIR)/*
	find ./ -name *.o -or -name *.bin -delete
