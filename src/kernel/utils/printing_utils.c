#include "printing_utils.h"

// ------------- PRIVATE ------------------

char four_bit_num_to_hex_char(u8 num) {
    if (num < 0xA) {
        return num+0x30;
    }else {
        return num+0x37;
    }
} 


// ------------ PUBLIC API ----------------

void u8_to_hex_str(u8 num, char *str) {
    *str = '0';
    *(str+1) = 'x';
    for (int i=4, j=2;i>=0 && j<4; i-=4, j++) {
        u8 four_bits = (num >> i) & 0xf;
        *(str+j) = four_bit_num_to_hex_char(four_bits);
    }
    // u8 num_h = (num >> 4) & 0xf;
    // u8 num_l = num & 0xf;
    // *(str+2) = four_bit_num_to_hex_char(num_h);
    // *(str+3) = four_bit_num_to_hex_char(num_l);
    *(str+4) = 0;
}

void u16_to_hex_str(u16 num, char *str) {
    *str = '0';
    *(str+1) = 'x';
    for (int i=12, j=2;i>=0 && j<6; i-=4, j++) {
        u8 four_bits = (num >> i) & 0xf;
        *(str+j) = four_bit_num_to_hex_char(four_bits);
    }
    // u8 byte1_h = (num >> 12) & 0xf;
    // u8 byte1_l = (num >> 8) & 0xf;
    // u8 byte0_h = (num >> 4) & 0xf;
    // u8 byte0_l = (num >> 0) & 0xf;
    // *(str+2) = four_bit_num_to_hex_char(byte1_h);
    // *(str+3) = four_bit_num_to_hex_char(byte1_l);
    // *(str+4) = four_bit_num_to_hex_char(byte0_h);
    // *(str+5) = four_bit_num_to_hex_char(byte0_l);
    *(str+6) = 0;
}

void u32_to_hex_str(u32 num, char *str) {
    *str = '0';
    *(str+1) = 'x';
    for (int i=28, j=2;i>=0 && j<10; i-=4, j++) {
        u8 four_bits = (num >> i) & 0xf;
        *(str+j) = four_bit_num_to_hex_char(four_bits);
    }
    // u8 byte3_h = (num >> 28) & 0xf;
    // u8 byte3_l = (num >> 24) & 0xf;
    // u8 byte2_h = (num >> 20) & 0xf;
    // u8 byte2_l = (num >> 16) & 0xf;
    // u8 byte1_h = (num >> 12) & 0xf;
    // u8 byte1_l = (num >> 8) & 0xf;
    // u8 byte0_h = (num >> 4) & 0xf;
    // u8 byte0_l = (num >> 0) & 0xf;
    // *(str+2) = four_bit_num_to_hex_char(byte3_h);
    // *(str+3) = four_bit_num_to_hex_char(byte3_l);
    // *(str+4) = four_bit_num_to_hex_char(byte2_h);
    // *(str+5) = four_bit_num_to_hex_char(byte2_l);
    // *(str+6) = four_bit_num_to_hex_char(byte1_h);
    // *(str+7) = four_bit_num_to_hex_char(byte1_l);
    // *(str+8) = four_bit_num_to_hex_char(byte0_h);
    // *(str+9) = four_bit_num_to_hex_char(byte0_l);
    *(str+10) = 0;
}

void u64_to_hex_str(u64 num, char *str) {
    *str = '0';
    *(str+1) = 'x';
    // u8 byte7_h = (num >> 60) & 0xf;
    // u8 byte7_l = (num >> 56) & 0xf;
    // u8 byte6_h = (num >> 52) & 0xf;
    // u8 byte6_l = (num >> 48) & 0xf;
    // u8 byte5_h = (num >> 44) & 0xf;
    // u8 byte5_l = (num >> 40) & 0xf;
    // u8 byte4_h = (num >> 36) & 0xf;
    // u8 byte4_l = (num >> 32) & 0xf;
    // u8 byte3_h = (num >> 28) & 0xf;
    // u8 byte3_l = (num >> 24) & 0xf;
    // u8 byte2_h = (num >> 20) & 0xf;
    // u8 byte2_l = (num >> 16) & 0xf;
    // u8 byte1_h = (num >> 12) & 0xf;
    // u8 byte1_l = (num >> 8) & 0xf;
    // u8 byte0_h = (num >> 4) & 0xf;
    // u8 byte0_l = (num >> 0) & 0xf;
    // *(str+2) = four_bit_num_to_hex_char(byte7_h);
    // *(str+3) = four_bit_num_to_hex_char(byte7_l);
    // *(str+4) = four_bit_num_to_hex_char(byte6_h);
    // *(str+5) = four_bit_num_to_hex_char(byte6_l);
    // *(str+6) = four_bit_num_to_hex_char(byte5_h);
    // *(str+7) = four_bit_num_to_hex_char(byte5_l);
    // *(str+8) = four_bit_num_to_hex_char(byte4_h);
    // *(str+9) = four_bit_num_to_hex_char(byte4_l);
    // *(str+10) = four_bit_num_to_hex_char(byte3_h);
    // *(str+11) = four_bit_num_to_hex_char(byte3_l);
    // *(str+12) = four_bit_num_to_hex_char(byte2_h);
    // *(str+13) = four_bit_num_to_hex_char(byte2_l);
    // *(str+14) = four_bit_num_to_hex_char(byte1_h);
    // *(str+15) = four_bit_num_to_hex_char(byte1_l);
    // *(str+16) = four_bit_num_to_hex_char(byte0_h);
    // *(str+17) = four_bit_num_to_hex_char(byte0_l);
    // *(str+18) = 0;

    for (int i=60, j=2;i>=0 && j<18; i-=4, j++) {
        u8 four_bits = (num >> i) & 0xf;
        *(str+j) = four_bit_num_to_hex_char(four_bits);
    }
}

void i8_to_str(i8 num, char *str) {
    if (num >> 7) {
        *(str) = '-';
        u8 num = 128 - (num & 0x80);
        
    } else {
    
    }
}

void i16_to_str(i16 num, char *str);
void i32_to_str(i32 num, char *str);
void i64_to_str(i64 num, char *str);
void f32_to_str(f32 num, char *str);
void f64_to_str(f64 num, char *str);
void ptr_to_str(void* ptr, char *str);
