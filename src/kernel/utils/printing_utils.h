
#ifndef _PRINTINT_UTILS__H_
#define _PRINTINT_UTILS__H_

#include "../drivers/screen_text_mode.h"
#include "../cpu/types.h"

void u8_to_hex_str(u8 num, char *str);
void u16_to_hex_str(u16 num, char *str);
void u32_to_hex_str(u32 num, char *str);
void u64_to_hex_str(u64 num, char *str);
void i8_to_str(i8 num, char *str);
void i16_to_str(i16 num, char *str);
void i32_to_str(i32 num, char *str);
void i64_to_str(i64 num, char *str);
void f32_to_str(f32 num, char *str);
void f64_to_str(f64 num, char *str);
void ptr_to_str(void* ptr, char *str);

#endif
