#ifndef _IDT__H_
#define _IDT__H_

#include "types.h"

#define KERNEL_CODE_SEGMENT 0x08

typedef struct {
    u16 base_lower_bits; // lower 16bits of handler function address
    u16 selector; // kernel segment selector
    u8 always0;
    u8 flags;
    /* First byte
     * Bit 7: "Interrupt is present"
     * Bits 6-5: Privilege level of caller (0=kernel..3=user)
     * Bit 4: Set to 0 for interrupt gates
     * Bits 3-0: bits 1110 = decimal 14 = "32 bit interrupt gate" */
    u16 base_higher_bits; // Higher 16bits of handler function address
} __attribute__((packed)) IDT_Gate;

typedef struct {
    u16 limit;
    u32 base;
} __attribute__((packed)) IDT_ptr;

#define IDT_ENTRIES 256

void set_idt_gate(u32 n, u32 handler_function_address);
void set_idt();

#endif
