#ifndef _IRQ__H_
#define _IRQ__H_


#include "types.h"
#include "../drivers/ports.h"

#define PIC_MASTER_COMMAND_PORT     0x20
#define PIC_MASTER_DATA_PORT        0x21 
#define PIC_SLAVE_COMMAND_PORT      0xA0
#define PIC_SLAVE_DATA_PORT         0xA1 

#define PIC_EOI                     0x20

/*  ICW are the initialisation commands given to the PICs
 *  the PIC reads 3 with the option of reading a fourth one
 *
 *  The first one(ICW1) says that the PIC is initialising
 *  the second one(ICW2) sets the vector(Interrupt Vector) offset which means where will the interrupt 0x0 be remaped to on the CPU
 *  the third one(ICW3) sets how the PIC is wired to each other as in who is the master and who is slave
 *  the fourth one(ICW4) gives extra data if needed
 * */

#define ICW1_ICW4                   0x01    // Indicates that ICW4 will be present
#define ICW1_SINGLE                 0x02    // Single (cascade) mode
#define ICW1_INTERVAL4              0x04    // call address interval 4(8)
#define ICW1_LEVEL                  0x08    // Level triggered (edge) mode
#define ICW1_INIT                   0x10    // Initialisation

#define ICW4_8086                   0x01    // 8086/8088 mode
#define ICW4_AUTO                   0x02    // auto (normal) EOI mode
#define ICW4_BUF_SLAVE              0x08    // Buffered mode slave
#define ICW4_BUF_MASTER             0x0C    // Buffered mode master
#define ICW4_SFNM                   0x10    // Special fully nested mode


void PIC_sendEOI(u8 irq);
void PIC_remap(u8 offset1, u8 offset2);


#endif
