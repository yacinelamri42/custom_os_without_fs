#include "idt.h"

IDT_Gate idt[IDT_ENTRIES];
IDT_ptr idt_reg;

void set_idt_gate(u32 n, u32 handler_function_address) {
    idt[n].base_lower_bits = (u16) handler_function_address & 0xffff;
    idt[n].selector = KERNEL_CODE_SEGMENT;
    idt[n].always0 = 0;
    idt[n].flags = 0x8e; // lower 5 bits needs to be 14 or 0xe
    idt[n].base_higher_bits = (u16) (handler_function_address >> 16) & 0xffff;
}   

void set_idt() {
    idt_reg.base = (u32) &idt;
    idt_reg.limit = IDT_ENTRIES * sizeof(IDT_Gate) - 1;
    __asm__ __volatile__ ("lidt (%0)" : : "r" (&idt_reg)); // idt_reg needs to be loaded, not idt
}

