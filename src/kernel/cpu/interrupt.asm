[extern isr_handler]
[extern irq_handler]

isr_common_stubs:
    pusha
    ; Interrupt service routines common function
    mov ax, ds
    push eax
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    call isr_handler

    pop eax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    popa
    add esp, 8
    sti
    iret


irq_common_stubs:
    pusha
    ; Interrupt service routines common function
    mov ax, ds
    push eax
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    call irq_handler

    pop ebx
    mov ds, bx
    mov es, bx
    mov fs, bx
    mov gs, bx
    popa
    add esp, 8
    sti
    iret


global isr0
isr0:
    cli
    push byte 0
    push byte 0
    jmp isr_common_stubs

global isr1

isr1:
    cli
    push byte 0
    push byte 1
    jmp isr_common_stubs
global isr2

isr2:
    cli
    push byte 0
    push byte 2
    jmp isr_common_stubs

global isr3

isr3:
    cli
    push byte 0
    push byte 3
    jmp isr_common_stubs

global isr4

isr4:
    cli
    push byte 0
    push byte 4
    jmp isr_common_stubs

global isr5

isr5:
    cli
    push byte 0
    push byte 5
    jmp isr_common_stubs

global isr6

isr6:
    cli
    push byte 0
    push byte 6
    jmp isr_common_stubs

global isr7

isr7:
    cli
    push byte 0
    push byte 7
    jmp isr_common_stubs

global isr8

isr8:
    cli
    push byte 0
    push byte 8
    jmp isr_common_stubs

global isr9

isr9:
    cli
    push byte 0
    push byte 9
    jmp isr_common_stubs

global isr10

isr10:
    cli
    push byte 0
    push byte 10
    jmp isr_common_stubs

global isr11

isr11:
    cli
    push byte 0
    push byte 11
    jmp isr_common_stubs

global isr12

isr12:
    cli
    push byte 0
    push byte 12
    jmp isr_common_stubs

global isr13

isr13:
    cli
    push byte 0
    push byte 13
    jmp isr_common_stubs

global isr14

isr14:
    cli
    push byte 0
    push byte 14
    jmp isr_common_stubs

global isr15

isr15:
    cli
    push byte 0
    push byte 15
    jmp isr_common_stubs

global isr16

isr16:
    cli
    push byte 0
    push byte 16
    jmp isr_common_stubs

global isr17

isr17:
    cli
    push byte 0
    push byte 17
    jmp isr_common_stubs

global isr18

isr18:
    cli
    push byte 0
    push byte 18
    jmp isr_common_stubs

global isr19

isr19:
    cli
    push byte 0
    push byte 19
    jmp isr_common_stubs

global isr20

isr20:
    cli
    push byte 0
    push byte 20
    jmp isr_common_stubs

global isr21

isr21:
    cli
    push byte 0
    push byte 21
    jmp isr_common_stubs

global isr22

isr22:
    cli
    push byte 0
    push byte 22
    jmp isr_common_stubs

global isr23

isr23:
    cli
    push byte 0
    push byte 23
    jmp isr_common_stubs

global isr24

isr24:
    cli
    push byte 0
    push byte 24
    jmp isr_common_stubs

global isr25

isr25:
    cli
    push byte 0
    push byte 25
    jmp isr_common_stubs

global isr26

isr26:
    cli
    push byte 0
    push byte 26
    jmp isr_common_stubs

global isr27

isr27:
    cli
    push byte 0
    push byte 27
    jmp isr_common_stubs

global isr28

isr28:
    cli
    push byte 0
    push byte 28
    jmp isr_common_stubs

global isr29

isr29:
    cli
    push byte 0
    push byte 29
    jmp isr_common_stubs

global isr30

isr30:
    cli
    push byte 0
    push byte 30
    jmp isr_common_stubs

global isr31

isr31:
    cli
    push byte 0
    push byte 31
    jmp isr_common_stubs


global irq0

irq0:
    cli
    push byte 0
    push byte 32
    jmp irq_common_stubs


global irq1

irq1:
    cli
    push byte 1
    push byte 33
    jmp irq_common_stubs



global irq2

irq2:
    cli
    push byte 2
    push byte 34
    jmp irq_common_stubs



global irq3

irq3:
    cli
    push byte 3
    push byte 35
    jmp irq_common_stubs



global irq4

irq4:
    cli
    push byte 4
    push byte 36
    jmp irq_common_stubs



global irq5

irq5:
    cli
    push byte 5
    push byte 37
    jmp irq_common_stubs



global irq6

irq6:
    cli
    push byte 6
    push byte 38
    jmp irq_common_stubs



global irq7

irq7:
    cli
    push byte 7
    push byte 39
    jmp irq_common_stubs



global irq8

irq8:
    cli
    push byte 8
    push byte 40
    jmp irq_common_stubs



global irq9

irq9:
    cli
    push byte 9
    push byte 41
    jmp irq_common_stubs



global irq10

irq10:
    cli
    push byte 10
    push byte 42
    jmp irq_common_stubs



global irq11

irq11:
    cli
    push byte 11
    push byte 43
    jmp irq_common_stubs



global irq12

irq12:
    cli
    push byte 12
    push byte 44
    jmp irq_common_stubs



global irq13

irq13:
    cli
    push byte 13
    push byte 45
    jmp irq_common_stubs



global irq14

irq14:
    cli
    push byte 14
    push byte 46
    jmp irq_common_stubs



global irq15

irq15:
    cli
    push byte 15
    push byte 47
    jmp irq_common_stubs


