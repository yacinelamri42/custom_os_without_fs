#ifndef _TYPES__H_
#define _TYPES__H_

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef float f32;
typedef double f64;

#define bool u8
#define true 1
#define false 0

#define size_t u32

#endif
