#include "timer.h"
#include "isr.h"

#define PIT_FREQ 1193180
#define PIT_MODE_CONTROL_PORT 0x43
#define PIT_MODE_DATA_PORT_0 0x40
#define PIT_MODE_DATA_PORT_1 0x41
#define PIT_MODE_DATA_PORT_2 0x42

u64 time = 0;

void timer_callback(Registers r) {
    clear();
    time++;
    kprint("Time: ");
    char time_str[40];
    u64_to_hex_str(time, time_str);
    kprint(time_str);
}

void init_timer(u32 freq) {
    u16 divisor = PIT_FREQ / freq;
    port_byte_out(PIT_MODE_CONTROL_PORT, 0b00110110);
    /*  MODE CONTROL DETAILS
	Bits
	 76 Counter Select Bits
	 00  select counter 0
	 01  select counter 1
	 10  select counter 2
	 11  read back command (8254 only, illegal on 8253, see below)

	Bits
	 54  Read/Write/Latch Format Bits
	 00  latch present counter value
	 01  read/write of MSB only
	 10  read/write of LSB only
	 11  read/write LSB, followed by write of MSB

	Bits
	321  Counter Mode Bits
	000  mode 0, interrupt on terminal count;  countdown, interrupt,
	     then wait for a new mode or count; loading a new count in the
	     middle of a count stops the countdown
	001  mode 1, programmable one-shot; countdown with optional
	     restart; reloading the counter will not affect the countdown
	     until after the following trigger
	010  mode 2, rate generator; generate one pulse after 'count' CLK
	     cycles; output remains high until after the new countdown has
	     begun; reloading the count mid-period does not take affect
	     until after the period
	011  mode 3, square wave rate generator; generate one pulse after
	     'count' CLK cycles; output remains high until 1/2 of the next
	     countdown; it does this by decrementing by 2 until zero, at
	     which time it lowers the output signal, reloads the counter
	     and counts down again until interrupting at 0; reloading the
	     count mid-period does not take affect until after the period
	100  mode 4, software triggered strobe; countdown with output high
	     until counter zero;  at zero output goes low for one CLK
	     period;  countdown is triggered by loading counter;  reloading
	     counter takes effect on next CLK pulse
	101  mode 5, hardware triggered strobe; countdown after triggering
	     with output high until counter zero; at zero output goes low
	     for one CLK period
     *
     * */
    set_irq_handler(IRQ0, timer_callback);
    u8 divisor_l = (u8)((divisor) & 0xff);
    u8 divisor_h = (u8)((divisor >> 8) & 0xff);

    port_byte_out(PIT_MODE_DATA_PORT_0, divisor_l); 
    port_byte_out(PIT_MODE_DATA_PORT_0, divisor_h); 

}
