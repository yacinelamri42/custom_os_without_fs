#ifndef _TIMER__H_
#define _TIMER__H_

#include "types.h"
#include "irq.h"
#include "isr.h"

void timer_callback(Registers r);
void init_timer(u32 freq);

#endif
