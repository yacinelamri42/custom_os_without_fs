#include "irq.h"

void PIC_sendEOI(u8 irq) {
    if (irq >= 8) {
        port_byte_out(PIC_SLAVE_COMMAND_PORT, PIC_EOI);
    }
    port_byte_out(PIC_MASTER_COMMAND_PORT, PIC_EOI);
}


/*  offset 1 is the offset for the master PIC
 *  offset 2 is the offset for the slave PIC
 * */
void PIC_remap(u8 offset1, u8 offset2) {
    u8 mask1, mask2;
    mask1 = port_byte_in(PIC_MASTER_DATA_PORT); // save the data in pics before
    mask2 = port_byte_in(PIC_SLAVE_DATA_PORT);

    // -------- ICW1 -------------
    port_byte_out(PIC_MASTER_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);
    port_io_wait();
    port_byte_out(PIC_SLAVE_COMMAND_PORT, ICW1_INIT | ICW1_ICW4);
    port_io_wait();
    // -------- ICW1 end ---------
    // -------- ICW2 -------------
    port_byte_out(PIC_MASTER_DATA_PORT, offset1);
    port_io_wait();
    port_byte_out(PIC_SLAVE_DATA_PORT, offset2);
    port_io_wait();
    // -------- ICW2 end ---------
    // -------- ICW3 -------------
    port_byte_out(PIC_MASTER_DATA_PORT, 0x4); // tell Master PIC that there is a slave PIC at IRQ2
    port_io_wait();
    port_byte_out(PIC_SLAVE_DATA_PORT, 0x2); // tell Slave PIC that it s a slave
    port_io_wait();
    // -------- ICW3 end --------
    // -------- ICW4 ------------
    port_byte_out(PIC_MASTER_DATA_PORT, ICW4_8086); // Master and slave PICs needs to run on 8086 mode and not 8080 mode
    port_io_wait();
    port_byte_out(PIC_SLAVE_DATA_PORT, ICW4_8086);
    port_io_wait();
    // ------- ICW4 end --------

    port_byte_out(PIC_MASTER_DATA_PORT, mask1);
    port_byte_out(PIC_SLAVE_DATA_PORT, mask2);
}
