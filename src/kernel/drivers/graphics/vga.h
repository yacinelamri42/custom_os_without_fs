#ifndef _VGA__H_
#define _VGA__H_

// IO ports for VGA
#define VGA_CRTC_CONTROLLER_ADDR 0x3B4
#define VGA_CRTC_CONTROLLER_DATA 0x3B5
#define VGA_GRAPHICS_CONTROLLER_ADDR 0x3CE
#define VGA_GRAPHICS_CONTROLLER_DATA 0x3CF
#define VGA_SEQUENCER_ADDR 0x3C4
#define VGA_SEQUENCER_DATA 0x3C5
#define VGA_ATTRIBUTE_ADDR_DATA 0x3C0 // Verify first if it is reading or writing to know if it s in Addr mode or Data mode
#define VGA_ATTRIBUTE_READ_DATA 0x3C1
#define VGA_MISCELLANEOUS_OUTPUT 0x3C2 // if this register is cleared, 0x3DA is mapped to 0x3BA and 0x3D4 is mapped to 0x3B4
#define VGA_MISCELLANEOUS_INPUT 0x3CC 

#endif
