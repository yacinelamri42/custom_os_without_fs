#ifndef __PORTS__H__
#define __PORTS__H__

#include "../cpu/types.h"

u8 port_byte_in(u16 port);
void port_byte_out(u16 port, u8 data);
u16 port_word_in(u16 port);
void port_word_out(u16 port, u16 data);
void port_io_wait();

#endif
