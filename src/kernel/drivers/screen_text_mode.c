#include "screen_text_mode.h"
#include "memory_manipulation.h"

// ------------ PRIVATE APIS START ----------------------

u16 get_cursor_position(void)
{
    u16 pos = 0;
    port_byte_out(VGA_REG_SCREEN_CTRL, 0x0F);
    pos |= port_byte_in(VGA_REG_SCREEN_DATA);
    port_byte_out(VGA_REG_SCREEN_CTRL, 0x0E);
    pos |= ((u16)port_byte_in(VGA_REG_SCREEN_DATA)) << 8;
    return pos;
}

void set_cursor_position(u8 col, u8 row) {
    u16 pos = row*VGA_TEXT_MODE_MAX_COLS + col;
    port_byte_out(VGA_REG_SCREEN_CTRL, 0x0f); // Sending the lower 8bits to VGA
    port_byte_out(VGA_REG_SCREEN_DATA, (u8) (pos & 0xff));
    port_byte_out(VGA_REG_SCREEN_CTRL, 0x0e); // Sending the higher 8bits to VGA
    port_byte_out(VGA_REG_SCREEN_DATA, (u8) ((pos>>8) & 0xff));
}

// Returns next row number
int print_char(char ch, VGA_16bit_colors fg, VGA_16bit_colors bg, u8 col, u8 row){
    if (ch == '\n') {
        set_cursor_position(0, row+1);
        return row+1;
    }
    u8 color = bg << 4 | fg;
    u32 pos = row*VGA_TEXT_MODE_MAX_COLS + col;
    u8 * vga_address = (u8*) VGA_TEXT_MODE_ADDR;
    *(vga_address + 2*pos) = ch;
    *(vga_address + 2*pos+1) = color;
    set_cursor_position(col, row);
    return row;
}

void scroll(u8 rows) {
    u16 line_buffer[160];
    for (int i=0;i<rows; i++) {
        for (u32 pos=160; pos<(2*VGA_TEXT_MODE_MAX_COLS*VGA_TEXT_MODE_MAX_ROWS); pos+=160) {
            memory_copy(line_buffer, VGA_TEXT_MODE_ADDR+(void*)pos, 160);
            memory_set(VGA_TEXT_MODE_ADDR+(void*)pos, 0, 160);
            memory_copy(VGA_TEXT_MODE_ADDR+(void*)pos-160, line_buffer,160);
        }
    }
}

// ------------ PRIVATE APIS END ----------------------

void kprint_at(char* message, VGA_16bit_colors fg, VGA_16bit_colors bg, i8 col, i8 row) {
    if (col < 0 || row < 0) {
        u32 pos = get_cursor_position();
        col = pos % VGA_TEXT_MODE_MAX_COLS;
        row = pos / VGA_TEXT_MODE_MAX_COLS;
    }
    while ((*message) != 0) {
        if (col >= VGA_TEXT_MODE_MAX_COLS) {
            row += col / VGA_TEXT_MODE_MAX_COLS;
            col = col % VGA_TEXT_MODE_MAX_COLS;
        }
        if (row >= VGA_TEXT_MODE_MAX_ROWS) {
            scroll(1);
            row = VGA_TEXT_MODE_MAX_ROWS-1;
        }
        if (print_char(*message, fg, bg, col, row) != row) {
            col = -1;
            row++;
        }
        message++;
        col++;
        set_cursor_position(col, row);
    }
}

void kprint(char *message) {
    kprint_at(message, 0xf, 0x0, -1, -1);
}

void clear() {
    char * video_addr_start = (char*) VGA_TEXT_MODE_ADDR;
    for (int i=0; i<2*VGA_TEXT_MODE_MAX_COLS*VGA_TEXT_MODE_MAX_ROWS; i+=2) {
        *(video_addr_start+i) = 0;
        *(video_addr_start+i+1) = 0;
    }
    set_cursor_position(0, 0);
}
