#ifndef _SCREEN_TEXT_MODE__H_
#define _SCREEN_TEXT_MODE__H_

#define VGA_TEXT_MODE_ADDR 0xb8000
#define VGA_TEXT_MODE_MAX_ROWS 25
#define VGA_TEXT_MODE_MAX_COLS 80

// VGA text mode colors
typedef enum {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    MAGENTA,
    BROWN,
    LIGHT_GREY,
    DARK_GREY,
    LIGHT_BLUE,
    LIGHT_GREEN,
    LIGHT_CYAN,
    LIGHT_RED,
    LIGHT_MAGENTA,
    LIGHT_BROWN,
    WHITE
} VGA_16bit_colors;


/* Screen i/o ports */
#define VGA_REG_SCREEN_CTRL 0x3d4
#define VGA_REG_SCREEN_DATA 0x3d5

#include "../cpu/types.h"
#include "ports.h"
#include "memory_manipulation.h"


// Public kernel api
void clear();
void kprint_at(char* message, VGA_16bit_colors fg, VGA_16bit_colors bg, i8 col, i8 row);
void kprint(char* message);

#endif

