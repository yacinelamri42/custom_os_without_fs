#include "memory_manipulation.h"

void * memory_copy(void *destination, const void *source, size_t n) {
    for (int i=0; i<n; i++) {
        *((u8*)(destination+i)) = *((u8*)(source+i));
    }
    return destination;
}

void * memory_set(void * destination, i8 num, size_t n) {
    for (int i=0; i<n; i++) {
        *((i8*)(destination+i)) = num;
    }
    return destination;
}
