#ifndef _MEMORY_MANIPULATION__H_
#define _MEMORY_MANIPULATION__H_

#include "../cpu/types.h"

void * memory_copy(void * destination, const void * source, size_t n);
void * memory_set(void * destination, i8 num, size_t n);

#endif
